import torch, tensorrt, torch_tensorrt
import torch.backends.cudnn as cudnn
# from torch2trt import torch2trt
from itertools import product
from time import time
from rich import print
from torchvision.models import resnet50

cudnn.benchmark = True
precisions = torch.float32
input_type = torch.float32

model = resnet50().eval().to(device=torch.device('cuda:0'), dtype=torch.float32)


result = []
x = torch.rand((8, 3, 224, 224), device=torch.device('cuda:0'), dtype=input_type)

script_model = torch.jit.script(model)
torch_tensorrt_model = torch_tensorrt.compile(model, 
                                              inputs=[torch_tensorrt.Input((8, 3, 224, 224), dtype=input_type)],
                                              enabled_precisions=precisions,
                                              workspace_size = 1 << 22)

script_trt_model = torch_tensorrt.compile(script_model,
                                            inputs=[torch_tensorrt.Input((8, 3, 224, 224), dtype=input_type)],
                                            enabled_precisions=precisions,
                                            workspace_size = 1 << 22)

for net in (model, torch_tensorrt_model, script_trt_model):
    out = net(x).detach().cpu()
    result.append(out)
    print(out)
    
    
for (i1, r1), (i2, r2) in product(enumerate(result), enumerate(result)):
    if i1 != i2:
        print('{} - {}'.format(i1, i2),' -> ' ,(r1-r2).round(decimals=8))
