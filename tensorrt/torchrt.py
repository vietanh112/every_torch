import torch, tensorrt
from torch2trt import torch2trt
from torchvision.models import resnet50
from time import time

model = resnet50(pretrained=False).eval().cuda()

# create example data
x = torch.ones((4, 3, 224, 224)).float().cuda()

# convert to TensorRT feeding sample data as input
model_trt = torch2trt(model, [x])

t = time()
y = model(x)#.detach()
tt = time()
print(y.argmax(), tt-t)

s = time()
y_trt = model_trt(x)#.detach()
e = time()
print(y_trt.argmax(), e-s)

