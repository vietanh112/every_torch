import torch, tensorrt, torch_tensorrt
import torch.backends.cudnn as cudnn
# from torch2trt import torch2trt
from torchinfo import summary
from time import time
from rich import print
from torchvision.models import resnet50



cudnn.benchmark = True
precisions = torch.float32
input_type = torch.float32
model = resnet50().eval().to(device=torch.device('cuda:0'), dtype=torch.float32)
# summary(model, input_size=(4, 3, 224, 224))


# x = torch.rand((8, 3, 224, 224), device=torch.device('cuda:0'), dtype=input_type)
# torch2trt_model = torch2trt(model, [x], fp16_mode=True)
torch_tensorrt_model = torch_tensorrt.compile(model, 
                                              inputs=[torch_tensorrt.Input((8, 3, 224, 224), dtype=input_type)],
                                              enabled_precisions=precisions,
                                              workspace_size = 1 << 22)

script_model = torch.jit.script(model)
script_trt_model = torch_tensorrt.compile(script_model,
                                              inputs=[torch_tensorrt.Input((8, 3, 224, 224), dtype=input_type)],
                                              enabled_precisions=precisions,
                                              workspace_size = 1 << 22)
freeze_model = torch.jit.freeze(script_model)
freeze_trt_model = torch_tensorrt.compile(freeze_model,
                                          inputs=[torch_tensorrt.Input((8, 3, 224, 224), dtype=input_type)],
                                              enabled_precisions=precisions,
                                              workspace_size = 1 << 22)

rt = [0., 0., 0.]
for _ in range(1000):
    x = torch.rand((8, 3, 224, 224), device=torch.device('cuda:0'), dtype=input_type)
    for i, net in enumerate((torch_tensorrt_model, script_trt_model, freeze_trt_model)):
        start = time()
        net(x)
        end = time()
        rt[i] += (end - start)

for i in rt:
    print(i)

# 64.25407910346985 nn.Module
# 22.07582664489746 torch2trt
# 10.964112997055054    torch_tensorrt
