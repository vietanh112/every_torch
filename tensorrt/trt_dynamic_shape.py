import torch, tensorrt, torch_tensorrt
from rich import print
from torchinfo import summary
from torchvision.models import resnet50


model = resnet50().eval().to(torch.device('cuda:0'))
summary(model, (1, 3, 224, 224)); exit()
size = (224, 224)
inputs = [
    torch_tensorrt.Input(
        min_shape=[1, 3, *size],
        opt_shape=[2, 3, *size],
        max_shape=[3, 3, *size],
        dtype=torch.float32,
    )
]

enabled_precisions = torch.float32

model = torch_tensorrt.compile(model,
                               inputs=inputs,
                               enabled_precisions=enabled_precisions)


