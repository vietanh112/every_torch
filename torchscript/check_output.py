import torch
from torch import nn, jit


def test_output():
    x = torch.tensor([1, 2, 3, 4, 5], dtype=torch.float32)

    fc = nn.Linear(5, 3, bias=True)
    print(fc.weight)
    print(fc(x))
    traced = jit.trace(fc, x)
    print(traced(x))

    scripted = jit.script(fc)
    print(scripted(x))
    

def test2():
    class MyDecisionGate(nn.Module):
        def forward(self, x):
            if x.sum() > 0:
                return x
            else:
                return -x

    class MyCell(nn.Module):
        def __init__(self, dg):
            super(MyCell, self).__init__()
            self.dg = dg
            self.linear = nn.Linear(4, 4)

        def forward(self, x, h):
            new_h = torch.tanh(self.dg(self.linear(x)) + h)
            return new_h, new_h
    
    
    x, h = torch.eye(3, 4), torch.eye(3, 4)
    my_cell = MyCell(MyDecisionGate())
    traced = jit.trace(my_cell, (x, h))
    scripted = jit.script(my_cell)
    print(x)
    print(h)
    print('=' * 80, end='\n\n')
    print('mycell = \n', my_cell(x, h))
    print('traced = \n', traced(x, h))
    print('scripted = \n', scripted(x, h))
    
# test2()

def test3():
    class MyDecisionGate(nn.Module):
        def forward(self, x):
            if x.sum() > 0:
                return x
            else:
                return -x

    class MyCell(nn.Module):
        def __init__(self, dg):
            super(MyCell, self).__init__()
            self.dg = dg
            self.linear = nn.Linear(4, 4)

        def forward(self, x, h):
            new_h = torch.tanh(self.dg(self.linear(x)) + h)
            return new_h, new_h
    
    
    x, h = torch.eye(3, 4), torch.eye(3, 4)
    gate = MyDecisionGate()
    my_cell = MyCell(gate)
    cell_state = my_cell.linear.state_dict()
    traced = jit.trace(my_cell, (x, h))
    
    scripted_gate = jit.script(gate)
    my_cell2 = MyCell(scripted_gate)
    my_cell2.linear.load_state_dict(cell_state)
    scripted = jit.script(my_cell2)
    print(x)
    print(h)
    print('=' * 80, end='\n\n')
    print('mycell = \n', my_cell(x, h))
    print('traced = \n', traced(x, h))
    print('scripted = \n', scripted(x, h))
    