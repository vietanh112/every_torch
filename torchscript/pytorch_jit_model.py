import torch, tensorrt, torch_tensorrt
import intel_extension_for_pytorch as ipex
from torch import nn, jit
from torch.jit import TopLevelTracedModule, TracedModule, RecursiveScriptModule, ScriptModule
from torchvision.models import resnet50
from torchinfo import summary
from time import time
from typing import Union



resnet = resnet50().eval()
summary(model= resnet, input_size=(2, 3, 224, 224))

data = torch.rand(8, 3, 224, 224)

scripted_resnet = jit.script(resnet)
intel_resnet = ipex.optimize(resnet)


def test_time(model:Union[nn.Module, TopLevelTracedModule, TracedModule, RecursiveScriptModule, ScriptModule], 
              device:torch.device= torch.device('cpu'), data:torch.Tensor = data) -> float:
    model.to(device=device)
    data= data.to(device=device)
    start = time()
    for _ in range(200):
        model(data)
    end = time()
    
    runtime = end - start
    print(runtime)
    return runtime

# print("GPU ============================== \n")
# test_time(resnet, torch.device('cuda:0'))
# test_time(scripted_resnet, torch.device('cuda:0'))


print("CPU ============================== \n")
test_time(resnet, torch.device('cpu'))
test_time(scripted_resnet, torch.device('cpu'))