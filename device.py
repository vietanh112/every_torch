import torch
from rich import print

print('torch version      ', torch.__version__)
print('cuda.is_available  ', torch.cuda.is_available())
print('cuda.device_count  ', torch.cuda.device_count())

current_device = torch.cuda.current_device()
print('cuda.current_device', current_device)
print(torch.cuda.get_device_name('cuda:{}'.format(current_device)))
