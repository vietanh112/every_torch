import torch


# class MyCustomSquare(torch.autograd.Function):
#     @staticmethod
#     def forward(ctx, input):
#         ctx.save_for_backward(input)
#         return input**2

#     @staticmethod
#     def backward(ctx, grad_output):
#         input, = ctx.saved_tensors
#         return 2*input*grad_output
    
# mysquare = MyCustomSquare.apply

# X = torch.tensor([[1, 2, 4],
#                 [1, 15, 11],
#                 [1, 4, -1]], requires_grad=False, dtype=torch.float32)

# W = torch.tensor([1., -3, 4.], requires_grad= True, dtype= torch.float32).reshape(3,1)
# y = torch.tensor([3, 2, -1]).reshape(3, 1)

# z = torch.matmul(X, W) -y
# z.backward(gradient= W)
# print(W.grad)
# L.backward(gradient=torch.tensor([1., -3, 3.]))
# print(W.grad)

X = torch.ones(size=(3,3), dtype= torch.float32, requires_grad=True)
y = 1.2*X + 100
y.backward(gradient= torch.tensor([[1, 2, 4],
                [2, 15, 11],
                [1, 4, 13]]))
print(X.grad)