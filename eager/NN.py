import torch
import torch.nn as nn
import torch.nn.functional as F

x = torch.tensor([1, 2, 3], dtype= torch.float32, requires_grad= True)

class Lenet(nn.Module):
    def __init__(self) -> None:
        super(Lenet, self).__init__()
        self.num_classes = 10
        # input 1 chanel, output 6 chanel, kernel size = 3x3
        self.conv1 = nn.Conv2d(in_channels= 1, out_channels= 32, kernel_size= 3, padding=1)
        self.conv2 = nn.Conv2d(in_channels= 32, out_channels= 32, kernel_size= 3, padding= 1)
        
        self.fc1 = nn.Linear(14*14*32, 128)             # layer 1 là flatten, layer 2 là 128 node
        self.fc2 = nn.Linear(128, self.num_classes)

    def forward(self, x):
        # max pooling 2x2
        x = F.relu(self.conv1(x))                   # inp:x -> conv -> relu                -> x
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)  # inp:x -> conv -> relu -> max_pooling -> x3
        # Flatten về vector
        x = torch.flatten(x, 1)                     # inp:x -> conv -> flatten             -> x
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        return x
        
# thiết kế mạng phân loại không có torch.softmax
model = Lenet()       # tạo model

# in ra các parameters trong model
# for layer in model.parameters():
#     print(layer)


# test feedforward
# shape batch_size * depth * height * width ( N * C * H * W)
inp = torch.randn(1, 1, 28, 28)
# print(inp.shape)
result = model(inp)
out = F.softmax(result)
class_index = torch.argmax(out)
print(result, torch.argmax(result), end='\n\n')
print(out, torch.argmax(out))
