import torch
from torch.utils.data import DataLoader
import torch.nn as nn
import torchvision
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda, Compose

# from torchviz import make_dot
import matplotlib.pyplot as plt
import os
import numpy as np

# print(torch.__version__)


class MySquare(torch.autograd.Function):

    @staticmethod
    def forward(ctx, input):
        ctx.save_for_backward(input)
        return input**2

    @staticmethod
    def backward(ctx, grad_output):
        input, = ctx.saved_tensors
        return 2*input*grad_output

my_square = MySquare.apply
x = torch.tensor([3])
y = torch.tensor([10])
a = torch.tensor([1.], requires_grad=True)
b = torch.tensor([2.], requires_grad=True)

yhat = a*x + b
z = yhat - y
L = my_square(z)

L.backward()
print(a.grad)
print(b.grad)