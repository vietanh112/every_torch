import os
import pandas as pd
from PIL import Image
from torch.utils.data import Dataset

label_map = {
    0:'',
    1:'',
    2:'',
}
# 

class CustomImageDataset(Dataset):
    def __init__(self, annotations_file, img_dir, transform=None, target_transform=None):
        self.img_labels = pd.read_csv(annotations_file)                 # file csv
        self.img_dir = img_dir
        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):          
        return len(self.img_labels)         # kích thước dataset

    def __getitem__(self, idx):
        img_path = os.path.join(self.img_dir, self.img_labels.iloc[idx, 0])
        image = Image.open(img_path)
        label = self.img_labels.iloc[idx, 1]
        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        sample = {"image": image, "label": label}
        return sample

class CustomDataset_CRNN(Dataset):
    def __init__(self, folder_img, transform= None, target_transform= None):
        self.folder_img = folder_img
        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):
        return len(os.listdir(self.folder_img))         # kích thước dataset

    def __getitem__(self, idx):
        list_img = sorted(os.listdir(self.folder_img))
        img_path = os.path.join(self.folder_img, list_img[idx])
        image = Image.open(img_path)                    # convert image to torch tensor
                                                        # lấy label trong tên ảnh
        label = list_img[0][33:].strip('_.jpg').replace('#', '/')
        
        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        sample = {"image": image, "label": label}
        return sample