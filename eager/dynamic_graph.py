import torch

x = torch.tensor([1., 2., 3.], requires_grad=True, dtype=torch.float32)
y = -2*x + 1
z = sum(y)
# retain_graph để cho phép tính đạo hàm nhiều lần
z.backward(retain_graph=True)
print(x.grad)  # 2, 2, 2
z.backward(retain_graph=True)       # nếu không thì graph sẽ bị hủy
print(x.grad)  # 4, 4, 4
z.backward(retain_graph=True)       # các biến .grad sẽ được cộng dồn
print(x.grad)  # 6, 6, 6
