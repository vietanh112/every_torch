def is_intel_cpu():
    from cpuinfo import get_cpu_info
    brand_raw = get_cpu_info()
    del get_cpu_info
    if 'intel' in brand_raw['brand_raw'].lower():
        if 'intel' in brand_raw['vendor_id_raw'].lower():
            return True
    return False
